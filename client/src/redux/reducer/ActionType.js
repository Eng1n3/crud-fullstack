const ActionType = {
    HANDLE_MESSAGE: 'HANDLE_MESSAGE',
    HANDLE_SUCCESS: 'HANDLE_SUCCESS'
}

export default ActionType;