const mongoose = require('mongoose');

const Person = mongoose.model('Person', {
    nama: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true
    },
    nomor: {
        type: String,
    }
})

module.exports = Person;