const Person = require('../models/person.js');
const validator = require('validator');
const { validatePost } = require("../validators/validate-contacts");
const Joi = require("joi");

exports.getContactsAll = async (req, res, next) => {
    try {
        const person = await Person.find();
        return res.status(200).json({
            message: "Berhasil mendapatkan data",
            data: person
        })
    } catch(err) {
        err.message = "Internal server error";
        next(err)
    }
}

exports.getContactsDetail = async (req, res, next) => {
    try {
        const person = await Person.findById(req.params.id);
        return res.status(200).json({
            message: "Berhasil mendapatkan data",
            data: person
        })
    } catch(err) {
        err.errorStatus = 400
        err.message = "Data tidak ada"
        next(err)
     }
}

exports.getContactsPost = async (req, res) => {
    try {
        const result = await validatePost.validateAsync(req.body);
        const duplikasiEmail = await Person.findOne({email: result.email});
        const duplikasiNomor = await Person.findOne({nomor: result.nomor});
        if (duplikasiEmail && duplikasiNomor) {
            throw ({details: [{message: "Email sudah digunakan!"}, {message: "Nomor sudah digunkan!"}]})
        } else if (duplikasiEmail) {
            throw ({details: [{message: "Email sudah digunakan!"}]})
        } else if (duplikasiNomor) {
            throw ({details: [{message: "Nomor sudah digunakan!"}]})
        }
        await Person.insertMany(req.body);
        return res.status(201).json({
            message: "Berhasil menambahkan data"
        })
    } catch (err) {
        res.status(422).json({
            message: "Invalid data",
            data: err.details
        })
    }
}

exports.getContactsPatch = async (req, res) => {
    try {
        const result = await validatePost.validateAsync(req.body);
        const duplikasiEmail = await Person.find({
            _id: {$ne: req.params.id}, 
            email: result.email
        });
        const duplikasiNomor = await Person.find({
            _id: {$ne: req.params.id}, 
            nomor: result.nomor
        });
        if ((duplikasiEmail.length !== 0) && (duplikasiNomor.length !== 0)) {
            throw ({details: [{message: "Email sudah digunakan!"}, {message: "Nomor sudah digunkan!"}]})
        } else if (duplikasiEmail.length !== 0) {
            throw ({details: [{message: "Email sudah digunakan!"}]})
        } else if (duplikasiNomor.length !== 0) {
            throw ({details: [{message: "Nomor sudah digunakan!"}]})
        }
        await Person.updateOne(
            {
                _id: req.params.id
            },
            {
                $set: {
                    nama: req.body.nama,
                    email: req.body.email,
                    nomor: req.body.nomor
                }
            }
        );
        return res.status(200).json({
            message: "Data berhasil diupdate"
        });
    } catch (err) {
        return res.status(422).json({
            message: "Invalid data",
            details: err.details
        })
    }
}

exports.getContactsDelete = async (req, res) => {
    try {
        await Person.deleteOne({_id:req.params.id})
        return res.status(200).json({
            message: "Data berhasil dihapus"
        });
    } catch(err) {
        return res.status(400).json({
            message: "Data tidak ada"
        });
    }
}