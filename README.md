# crud-fullstack

set database:
- ./server/config/db

Install package:
- ./server
- npm install

- ./client
- npm install

Running server:
- ./server
- npm start

- ./client
- npm start
