const express = require("express");
const { 
    getContactsAll,
    getContactsDelete,
    getContactsDetail,
    getContactsPatch,
    getContactsPost
} = require("../controlers/contacts.js");
const router = express.Router();

/**
 * @swagger
 * components:
 *  schemas:
 *      Contact:
 *          type: object
 *          properties:
 *              message:
 *                  type: string
 *                  example: Berhasil mendapatkan data
 *              data:
 *                  type: array
 *                  items:
 *                     type: object
 *                     required:
 *                      - nama
 *                      - email
 *                     properties:
 *                      _id:
 *                          type: string
 *                          example: 6197934a868b975d9939df41
 *                      nama:
 *                          type: string
 *                          example: Jay
 *                      email:
 *                          type: string
 *                          example: jay@gmail.com
 *                      nomor:
 *                          type: string
 *                          example: "089630682130"
 *                      __v:
 *                          type: integer
 *                          example: 0
 */




/**
 * @swagger
 * tags:
 *  name: Contacts
 *  description: mengatur contact API
 */

/**
 * @swagger
 * /contacts:
 *  get:
 *      summary: List semua contact
 *      tags: [Contacts]
 *      responses:
 *          200:
 *              description: OK
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/Contact'
 *          500:
 *              description: Bad Request
 */

router.get('/', getContactsAll);

/**
 * @swagger
 * /contacts/{userId}:
 *  get:
 *      summary: Data contact dengan id
 *      parameters:
 *          - in: path
 *            name: userId
 *            required: true
 *      tags: [Contacts]
 *      responses:
 *          200:
 *              description: OK
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/Contact'
 *          400:
 *              description: Bad Request
 *          500:
 *              description: Internal server error
 */

router.get('/:id', getContactsDetail);

/**
 * @swagger
 * /contacts:
 *  post:
 *      summary: Creates a new user.
 *      tags: [Contacts]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          nama:
 *                              type: string
 *                          email:
 *                              type: string
 *                          nomor:
 *                              type: string
 *                      required:
 *                          - nama
 *                          - email
 *      responses:
 *          201:
 *              description: Created
 *              content:
 *                  applcation/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              message:
 *                                  type: string
 *                                  example: Data berhasil ditambahkan
 *          422:
 *              description: Invalid data
 */

router.post('/', getContactsPost);

/**
 * @swagger
 * /contacts/{userId}:
 *  patch:
 *      summary: Updates a user.
 *      tags: [Contacts]
 *      parameters:
 *          - in: path
 *            name: userId
 *            required: true
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      required:
 *                          - nama
 *                          - email
 *                      properties:
 *                          nama:
 *                              type: string
 *                              example: John Doe
 *                          email:
 *                              type: string
 *                              example: johndoe@gmail.com
 *                          nomor:
 *                              type: string
 *                              example: "089630682143"
 *      responses:
 *          200:
 *              description: OK
 *          422:
 *              description: Invalid data
 *          500:
 *              description: Internal server error
 */

router.patch('/:id', getContactsPatch);

/**
 * @swagger
 * /contacts/{userId}:
 *  delete:
 *      summary: Delete a user.
 *      tags: [Contacts]
 *      parameters:
 *          - in: path
 *            name: userId
 *            required: true
 *      responses:
 *          200:
 *              description: OK
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              message:
 *                                  type: string
 *                                  example: Data berhasil dihapus
 *          400:
 *              description: Bad Request
 *          500:
 *              description: Internal server error
 */

router.delete('/:id', getContactsDelete);

module.exports = router;