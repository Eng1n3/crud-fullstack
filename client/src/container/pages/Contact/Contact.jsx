import React, {Component, Fragment} from "react";
import ListContact from "../../../component/ListContact/ListContact";
import {connect} from "react-redux";
import ActionType from "../../../redux/reducer/ActionType";



class Contact extends Component {
    constructor(props) {
        super(props)
        this.state = {
            posts: []
        }
    }

    getDataAPI = async () => {
        await fetch("http://localhost:8000/contacts")
        .then(response => response.json())
        .then(response => {
            console.log(response)
            this.setState({
                posts: response.data
            })
        })
        .catch(err => console.error(err));
    }

    goUrlDetail = id => {
        return this.props.history.push(`/detail-post/${id}`);
    }

    goUrlCreate = () => {
        return this.props.history.push(`/create-post`);
    }

    componentDidMount() {
        this.getDataAPI();
    }

    componentWillUnmount() {
        this.props.handleSuccess("")
    }

    render() {
        return (
            <Fragment>
                <div className="container">
                    <h1>Ini contact page</h1>
                    <div className="row">
                        <div className="col-md-12">
                            {
                                this.props.success &&
                                    <div className="alert alert-success" role="alert">
                                        {this.props.success}
                                    </div>
                            }
                            <button onClick={this.goUrlCreate} className="btn btn-md btn-primary">Tambahkan data</button>
                            <div className="table-responsive">
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Nama</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Nomor</th>
                                            <th scope="col">Detail</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.posts.map((post, i) => {
                                                return (
                                                    <ListContact 
                                                        key={post._id}
                                                        no={i+1}
                                                        data={post}
                                                        getId={this.goUrlDetail}
                                                    />
                                                )
                                            })
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        success: state.success
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        handleSuccess: (value) => dispatch({type: ActionType.HANDLE_SUCCESS, newValue: value})

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Contact);