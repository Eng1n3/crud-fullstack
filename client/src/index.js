import React from 'react';
import ReactDOM from 'react-dom';
import Base from "./container/Base/Base";
import {createStore} from 'redux';
import { Provider } from 'react-redux';
import rootReducer from './redux/reducer/globalState';


const storeRedux = createStore(rootReducer)

ReactDOM.render(
  <Provider store={storeRedux}>
    <Base />
  </Provider>,
  document.getElementById('root')
);
