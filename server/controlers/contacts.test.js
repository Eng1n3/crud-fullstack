const request = require("supertest");
const express = require("express");
const mongoose = require("mongoose")
const Person = require("../models/person.js")
const contactRoutes = require("../routes/contacts.js");


const app = new express();
app.use(express.json())

app.use("/contacts", contactRoutes)

beforeAll(async () => {
  const url = `mongodb://127.0.0.1/unit-test`
  await mongoose.connect(url, { useNewUrlParser: true })
  const person = new Person({
    nama: "Adam Brilian",
    email: "adam@gmail.com",
    nomor: "089630682130"
  })
  return await person.save();
})
afterAll(async () => {
  await Person.deleteMany();
  return await mongoose.connection.close()
})

describe("Test contacts routes", () => {

  test('POST /contacts', async () => {
    const count = await Person.count();
    const result = await request(app).post("/contacts").send({
      nama: "Testimoni",
      email: "testimoni@gmail.com",
      nomor: "089630682133"
    })
    const newCount = await Person.count()
    expect(newCount).toBe(count + 1);
    expect(result.statusCode).toEqual(201);
    expect(result.header['content-type']).toEqual("application/json; charset=utf-8")
  })
  test('Duplikasi Email POST /contacts', async () => {
    const count = await Person.count();
    const result = await request(app).post("/contacts").send({
      nama: "Testimoni",
      email: "testimoni@gmail.com",
      nomor: "089630682137"
    })
    const data = JSON.parse(result.text)
    const newCount = await Person.count()
    expect(newCount).toBe(count);
    expect(result.statusCode).toEqual(422);
    expect(result.header['content-type']).toEqual("application/json; charset=utf-8");
  })
  test('Duplikasi Nomor POST /contacts', async () => {
    const count = await Person.count();
    const result = await request(app).post("/contacts").send({
      nama: "Testimoni",
      email: "testi@gmail.com",
      nomor: "089630682133"
    })
    const data = JSON.parse(result.text)
    const newCount = await Person.count()
    expect(newCount).toBe(count);
    expect(result.statusCode).toEqual(422);
    expect(result.header['content-type']).toEqual("application/json; charset=utf-8");
  })
  test('Duplikasi semua request body POST /contacts', async () => {
    const count = await Person.count();
    const result = await request(app).post("/contacts").send({
      nama: "Testimoni",
      email: "testimoni@gmail.com",
      nomor: "089630682133"
    })
    const data = JSON.parse(result.text)
    const newCount = await Person.count()
    expect(newCount).toBe(count);
    expect(result.statusCode).toEqual(422);
    expect(result.header['content-type']).toEqual("application/json; charset=utf-8");
  })
  test("ERROR POST /contacts", async () => {
    const count = await Person.count();
    const result = await request(app).post("/contacts").send({
        nama: "Testimoni",
        email: "testimoni",
        nomor: "089"
    })
    const data = JSON.parse(result.text)
    const newCount = await Person.count()
    expect(newCount).toBe(count);
    expect(result.statusCode).toEqual(422);
    expect(result.header['content-type']).toEqual("application/json; charset=utf-8");
  })
  test("Kirim data kosong /contacts/:id", async () => {
    const count = await Person.count();
    const result = await request(app).post("/contacts").send({
        nama: "",
        email: "",
        nomor: ""
    })
    const data = JSON.parse(result.text)
    const newCount = await Person.count()
    expect(newCount).toBe(count);
    expect(result.statusCode).toEqual(422);
    expect(result.header['content-type']).toEqual("application/json; charset=utf-8");
  })
})

describe("GET /contacts", () => {
  test("Get /contacts", async () => {
    const result = await request(app).get("/contacts");
    const data = JSON.parse(result.text)
    expect(result.header['content-type']).toEqual("application/json; charset=utf-8");
    expect(result.statusCode).toEqual(200);
    expect(data.message).toEqual("Berhasil mendapatkan data");
  })
})

describe("ERROR GET /contacts", () => {
  beforeEach(async () => {
    return await mongoose.connection.close();
  })
  test("ERROR GET /contacts", async () => {
    const result = await request(app).get("/contacts");
    expect(result.header['content-type']).toEqual("text/html; charset=utf-8");
   expect(result.statusCode).toEqual(500);
  })
  afterEach( async () => {
    const url = `mongodb://127.0.0.1/unit-test`
    return await mongoose.connect(url, { useNewUrlParser: true })
  })
})

describe("GET /contacts/:id", () => {
  test("GET /contacts/:id", async () => {
    const person = await Person.find();
    const id = person[0]._id;
    const result = await request(app).get(`/contacts/${id}`);
    const data = JSON.parse(result.text)
    expect(result.header['content-type']).toEqual("application/json; charset=utf-8");
    expect(result.statusCode).toEqual(200);
    expect(data.message).toEqual("Berhasil mendapatkan data");
  })
  test("ERROR GET /contacts/:id", async () => {
    const result = await request(app).get("/contacts/id-asal");
    expect(result.header['content-type']).toEqual("text/html; charset=utf-8");
    expect(result.statusCode).toEqual(500);
  })
})

describe("Test PATCH", () => {
  test("PATCH /contacts/:id", async () => {
    const person = await Person.find();
    const id = person[1]._id;
    const result = await request(app).patch(`/contacts/${id}`).send({
      nama: "Testimoni Update",
      email: "testimoniupdate@gmail.com",
      nomor: "089630682133"
    });
    const text = JSON.parse(result.text);
    const message = text.message;
    expect(result.header['content-type']).toEqual('application/json; charset=utf-8');
    expect(result.statusCode).toEqual(200);
    expect(message).toEqual("Data berhasil diupdate");
  })
  test("ERROR PATCH /contacts/:id", async () => {
    const result = await request(app).patch(`/contacts/id-asal`).send({
      nama: "Testimoni Update",
      email: "testimoniupdate@gmail.com",
      nomor: "089630682133"
    });
    const text = JSON.parse(result.text);
    const message = text.message;
    expect(result.header['content-type']).toEqual('application/json; charset=utf-8');
    expect(result.statusCode).toEqual(422);
    expect(message).toEqual("Invalid data");
  })
  test('Duplikasi Email PATCH /contacts/:id', async () => {
    const person = await Person.find();
    const id = person[1]._id;
    const count = await Person.count();
    const result = await request(app).patch(`/contacts/${id}`).send({
      nama: "Testimoni",
      email: "adam@gmail.com",
      nomor: "089630682135"
    })
    const data = JSON.parse(result.text)
    const newCount = await Person.count()
    expect(newCount).toBe(count);
    expect(result.statusCode).toEqual(422);
    expect(result.header['content-type']).toEqual("application/json; charset=utf-8");
  })
  test('Duplikasi Nomor PATCH /contacts/:id', async () => {
    const person = await Person.find();
    const id = person[1]._id;
    const count = await Person.count();
    const result = await request(app).patch(`/contacts/${id}`).send({
      nama: "Testimoni",
      email: "testi@gmail.com",
      nomor: "089630682130"
    })
    const newCount = await Person.count()
    expect(newCount).toBe(count);
    expect(result.statusCode).toEqual(422);
    expect(result.header['content-type']).toEqual("application/json; charset=utf-8");
  })
  test("Duplikasi data patch", async () => {
    const count = await Person.count();
    const person = await Person.find();
    const id = person[1]._id;
    const result = await request(app).patch(`/contacts/${id}`).send({
      nama: "Testimoni Update",
      email: "adam@gmail.com",
      nomor: "089630682130"
    });
    const newCount = await Person.count()
    expect(newCount).toBe(count);
    expect(result.statusCode).toEqual(422);
    expect(result.header['content-type']).toEqual("application/json; charset=utf-8");
  })
  test("Request tidak valid patch", async () => {
    const count = await Person.count();
    const person = await Person.find();
    const id = person[1]._id;
    const result = await request(app).patch(`/contacts/${id}`).send({
      nama: "Testimoni Update",
      email: "testi@",
      nomor: "02130"
    });
    const data = JSON.parse(result.text)
    const newCount = await Person.count()
    expect(newCount).toBe(count);
    expect(result.statusCode).toEqual(422);
    expect(result.header['content-type']).toEqual("application/json; charset=utf-8");
  })
  test("Kirim data kosong", async () => {
    const count = await Person.count();
    const person = await Person.find();
    const id = person[1]._id;
    const result = await request(app).patch(`/contacts/${id}`).send({
      nama: "",
      email: "",
      nomor: ""
    });
    const data = JSON.parse(result.text)
    const newCount = await Person.count()
    expect(newCount).toBe(count);
    expect(result.statusCode).toEqual(422);
    expect(result.header['content-type']).toEqual("application/json; charset=utf-8");
  })
})


describe("Test DELETE", () => {
  test("DELETE /contacts/:id", async () => {
    const person = await Person.find();
    const id = person[0]._id;
    const result = await request(app).delete(`/contacts/${id}`);
    const text = JSON.parse(result.text);
    const message = text.message;
    expect(result.header['content-type']).toEqual('application/json; charset=utf-8');
    expect(result.statusCode).toEqual(200);
    expect(message).toEqual("Data berhasil dihapus");
  })
  test("DELETE /contacts/:id", async () => {
    const result = await request(app).delete(`/contacts/id-asal`);
    const text = JSON.parse(result.text);
    const message = text.message;
    expect(result.header['content-type']).toEqual('application/json; charset=utf-8');
    expect(result.statusCode).toEqual(400);
    expect(message).toEqual("Data tidak ada");
  })
})