const validator = require('validator');
const {body, check} = require('express-validator');
const Person = require('../models/person.js');

exports.validatePost = [
    check('email', 'Email tidak valid!').isEmail(),
    body('email').custom( async (value) => {
        const duplikat = await Person.findOne({email: value});
        if ( duplikat ) {
            throw new Error('Email sudah digunakan!');
        }
        return true;
    }),
    body('nomor').custom( async (value) => {
        if (value) {
            const duplikat = await Person.findOne({nomor: value});
            if ( duplikat ) {
                throw new Error('Nomor sudah digunakan!');
            } else if ( !validator.isMobilePhone(value, 'id-ID') ) {
                throw new Error('Nomor tidak valid!');
            }
        }
        return true;
    })
]

exports.validatePatch = [
    check('email', 'Email tidak valid!').isEmail(),
    body('email').custom( async (value, {req}) => {
        const duplikat = await Person.find({
            _id: { $ne: req.params.id },
            email: value
        });
        if ( duplikat.length !== 0 ) {
            throw new Error('Email sudah digunakan!');
        }
        return true;
    }),
    body('nomor').custom( async (value, {req}) => {
        if (value) {
            const duplikat = await Person.find({
                _id: { $ne: req.params.id },
                nomor: value
            });
            if ( duplikat.length !== 0 ) {
                throw new Error('Nomor sudah digunakan!');
            } else if ( !validator.isMobilePhone(value, 'id-ID') ) {
                throw new Error('Nomor tidak valid!');
            }
        }
        return true;
    })
]