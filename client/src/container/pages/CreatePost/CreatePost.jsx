import React, {Component, Fragment} from "react";
import ActionType from "../../../redux/reducer/ActionType";
import {connect} from "react-redux";

class CreatePost extends Component {
    constructor(props) {
        super(props);
        this.state = {
            post: {
                nama: "",
                email: "",
                nomor: ""
            },
        }
    }

    postAPI = async () => {
        await fetch('http://localhost:8000/contacts', {
            method: 'POST',
            body: JSON.stringify(this.state.post),
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            }
        })
        .then(response => response.json())
        .then(response => {
            console.log(response)
            if (response.details) {
                return this.props.handleMessage(response.details)
            }
            this.props.handleSuccess(response.message)
            return this.props.history.push('/contact');
        })
        .catch(err => console.error(err));
    }

    handleForm = event => {
        const newPost = {...this.state.post};
        const id = event.target.id;
        switch (id) {
            case "nama":
                newPost.nama = event.target.value;
                break;
            case "email":
                newPost.email = event.target.value;
                break;
            case "nomor":
                newPost.nomor = event.target.value;
                break;
            default:
                break;
        }
        this.setState({
            post: newPost
        });
    }

    handleSubmit = event => {
        event.preventDefault();
        this.postAPI();
    }
 
    componentWillUnmount() {
        this.props.handleMessage([])
    }

    render() {
        return (
            <Fragment>
                <div className="container">
                    <h1>Create dan Update Data</h1>
                    <div className="row">
                        <div className="col-md-6">
                            {
                                this.props.message.length !== 0 &&
                                    <div className="alert alert-danger" role="alert">
                                        <ul>
                                            {
                                                this.props.message.map((msg, index) => {
                                                    return <li key={index}>{msg.message}</li>
                                                })
                                            }
                                        </ul>
                                    </div>
                            }
                            <form onSubmit={this.handleSubmit}>
                                <div className="mb-3">
                                    <label htmlFor="nama" className="form-label">Nama</label>
                                    <input onChange={this.handleForm} value={this.state.post.nama} type="text" className="form-control" id="nama" name="nama" required />
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="email" className="form-label">Email</label>
                                    <input onChange={this.handleForm} value={this.state.post.email} type="email" className="form-control" id="email" name="email" required />
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="nomor" className="form-label">Nomor handphone</label>
                                    <input onChange={this.handleForm} value={this.state.post.nomor} type="nomor" className="form-control" id="nomor" name="nomor" required />
                                </div>
                                <button className="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        message: state.message,
        success: state.success
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        handleMessage: (value) => dispatch({type: ActionType.HANDLE_MESSAGE, newValue: value}),
        handleSuccess: (value) => dispatch({type: ActionType.HANDLE_SUCCESS, newValue: value})

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreatePost);