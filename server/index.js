const express = require('express');
const db = require('./config/db.js');
const contactRoutes = require('./routes/contacts');
const cors = require('cors');
require("dotenv").config();
const app = express();
const port = process.env.port;
const swaggerUI = require("swagger-ui-express");
const swaggerJsDoc = require("swagger-jsdoc");

const options = {
   definition: {
      openapi: "3.0.0",
      info: {
         title: "Contacts API",
         version: "1.0.0",
         description: "API contacts"
      },
      servers: [
         {
            url: "http://localhost:8000"
         }
      ]
   },
   apis: ["./routes/*.js"]
}

const specs = swaggerJsDoc(options);

app.use(cors());
app.use(express.json());

app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(specs));

app.use("/contacts", contactRoutes);

app.use((err, req, res, next) => {
   const status = err.errorStatus || 500
   const message = err.message
   const data = err.data
   return res.status(status).json({
      message: message,
      data: data
   })
})

db
 .then(response => {
    app.listen(port, () => console.log(`Server is running at http://localhost:${port}`))
 })
 .catch(err => console.error(err));