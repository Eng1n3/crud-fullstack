GET http://localhost:8000/contacts

###
POST http://localhost:8000/contacts
Content-Type: application/json

{
    "nama": "Adam Brilian",
    "email": "adam@gmail.com",
    "nomor": "089630682139"
}

###
GET http://localhost:8000/contacts/61960cdd1a847f87c3a

###
PATCH http://localhost:8000/contacts/61960cdd1a847f87c3a2dfbe
Content-Type: application/json

{
    "nama": "Fool",
    "email": "fool@gmail.com",
    "nomor": "089630682130"
}