const Joi = require("joi");

const validatePost = Joi.object({
    nama: Joi.string().min(1).max(50).required(),
    email: Joi.string().email().lowercase().required(),
    nomor: Joi.string().min(11).max(15).allow("")
})

module.exports = {
    validatePost
}