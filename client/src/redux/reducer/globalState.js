const globalState = {
    message: [],
    success: ""
}

const rootReducer = (state = globalState, action) => {
    if ( action.type === 'HANDLE_MESSAGE') {
        return {
            ...state,
            message: action.newValue
        }
    }
    if ( action.type === 'HANDLE_SUCCESS') {
        return {
            ...state,
            success: action.newValue
        }
    }
    return state;
}

export default rootReducer;